using Microsoft.EntityFrameworkCore;
using TestProject.Models;

namespace TestProject.Data;

public class StoreContext: DbContext
{
    public DbSet<Category> Categories { get; set; }
    public DbSet<Product> Products { get; set; }

    public StoreContext(DbContextOptions options) : base(options)
    {
    }
}