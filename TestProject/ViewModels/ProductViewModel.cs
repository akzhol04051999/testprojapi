using System.Dynamic;

namespace TestProject.ViewModels;

public class ProductViewModel: DynamicObject
{
    public string Name { get; set; }
    public string Description { get; set; }
    public double Price { get; set; }
    public int CategoryId { get; set; }
    public dynamic OtherFields { get; set; } = new Dictionary<string, string>();
}