using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestProject.Models;
using TestProject.Data;

namespace TestProject.Controllers;

[ApiController]
[Route("api/[Controller]")]

public class CategoryController: ControllerBase
{
    private readonly StoreContext _context;

    public CategoryController(StoreContext context)
    {
        _context = context;
        
        //Для заполнения БД при запуске приложения, если оно не было заполнено
        if (!_context.Categories.Any())
        {
            _context.Categories.Add(new Category { Name = "Сматрфоны", Fields = "Количество ядер:;Цвет:"});
            _context.Categories.Add(new Category { Name = "Холодильники", Fields = "Вес:;Высота:"});
            _context.Categories.Add(new Category { Name = "Ноутбуки", Fields = "Тип матрицы:"});

            _context.SaveChanges();
        }
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Category>>> Get() => await _context.Categories.ToListAsync();

    [HttpGet("{id}")]
    public async Task<ActionResult<Category>> Get(int id)
    {
        var category = await _context.Categories.FirstOrDefaultAsync(c => c.Id == id);
        if (category is null)
            return NotFound();
        return new ObjectResult(category);
    }

    
    //После каждого нового поля, используйте:  ';' точку запятой.
    [HttpPost]
    public async Task<ActionResult<Category>> Post(Category category)
    {
        if (_context.Categories.Any(c => c == category) || category is null)
            return BadRequest();
        
        _context.Categories.Add(category);
        await _context.SaveChangesAsync();
        return Ok(category);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Category>> Delete(int id)
    {
        Category category = _context.Categories.FirstOrDefault(c => c.Id == id);
        if (category is null)
            return NotFound();

        _context.Categories.Remove(category);
        await _context.SaveChangesAsync();
        return Ok(category);
    }
}