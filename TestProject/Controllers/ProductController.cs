using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestProject.Data;
using TestProject.Models;
using TestProject.Services;
using TestProject.ViewModels;

namespace TestProject.Controllers;

[ApiController]
[Route("api/[Controller]")]
public class ProductController: ControllerBase
{
    private readonly StoreContext _context;

    public ProductController(StoreContext context)
    {
        _context = context;
        if (!_context.Products.Any())
        {
            _context.Products.Add(new Product
            {
                Name = "Samsung s23", Description = "Лучший телефон на рынке",
                Price = 340000, CategoryId = 1, OtherFields = "8; Темно зеленый"
            });
            _context.SaveChanges();
        }
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ProductViewModel>>> Get()
    {
        var products = await _context.Products.Include(p => p.Category).ToListAsync();
        var models = new List<ProductViewModel>();
        foreach (var product in products)
        {
            models.Add(new ProductViewModel
            {
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                CategoryId = product.CategoryId,
                OtherFields = DataServices.SetDictionary(product.Category.Fields, product.OtherFields)
            });
        }

        return models;
    }

    [HttpGet("info/{id}")]
    public async Task<ActionResult<ProductViewModel>> ProductInfo(int id)
    {
        var product = await _context.Products.Include(p => p.Category).FirstOrDefaultAsync(p => p.Id == id);
        if (product is null)
            return NotFound();
        var model = new ProductViewModel()
        {
            Name = product.Name,
            Description = product.Description,
            Price = product.Price,
            CategoryId = product.CategoryId,
            OtherFields = DataServices.SetDictionary(product.Category.Fields, product.OtherFields)
        };
        return new ObjectResult(model);
    }

    
    [HttpGet("filter/{categoryId}")]
    public async Task<ActionResult<IEnumerable<ProductViewModel>>> ProductFilter(int categoryId)
    {
        var products = await _context.Products.Include(p => p.Category).Where(p => p.CategoryId == categoryId).ToListAsync();

        var models = new List<ProductViewModel>();
        foreach (var product in products)
        {
            models.Add(new ProductViewModel
            {
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                CategoryId = product.CategoryId,
                OtherFields = DataServices.SetDictionary(product.Category.Fields, product.OtherFields)
            });
        }
        return new ObjectResult(models);
    }

    [HttpPost]
    //После каждого нового поля, используйте:  ';' точку запятой.
    public async Task<ActionResult<Product>> Post(Product product)
    {
        if (_context.Products.Any(p => p == product) || product is null)
            return BadRequest();

        _context.Products.Add(product);
        await _context.SaveChangesAsync();
        return new ObjectResult(product);
    }
}