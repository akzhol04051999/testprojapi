using System.Text.Json.Serialization;

namespace TestProject.Models;

public class Category
{
    [JsonIgnore]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Fields { get; set; } = null;
}