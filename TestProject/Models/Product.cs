using System.Text.Json.Serialization;
using Swashbuckle.AspNetCore.Annotations;

namespace TestProject.Models;

public class Product
{
    [JsonIgnore]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public double Price { get; set; }
    public string OtherFields { get; set; } = null;
    [JsonIgnore]
    public Category? Category { get; set; }
    public int CategoryId { get; set; }
}