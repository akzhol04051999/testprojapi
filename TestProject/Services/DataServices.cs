namespace TestProject.Services;

public class DataServices
{
    public static string SetFields(Dictionary<string, string> dictionary)
    {
        string otherFields = null;
        foreach (var dict in dictionary)
        {
            otherFields += dict.Value + " ";
        }
        return otherFields;
    }

    public static Dictionary<string, string> SetDictionary(string key, string value)
    {
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        var keys = key.Split(";");
        var values = value.Split(";");
        for (int i = 0; i < keys.Length; i++)
        {
            dictionary.Add(keys[i], values[i]);
        }

        return dictionary;
    }
}